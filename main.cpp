/*
	Name: Chris Hurst
	Class: CSC160

	Files: airline.cpp
	Description: This program is a simple airline booking service, where it is menu driven. Each option is mapped
				so that it corresponds to its counter-part of the program. It will as the user for the flyer's
				information and store that info in the array and when called it will print the manifest of the 
				all the passengers of the plane. 
*/
#include <iostream>
#include <iomanip>
#include <conio.h>

using namespace std;

const int maxrow = 5, maxcol = 3;

struct flyerinfotype{
    string fname;
    string lname;
    string id;
    int numbags;
    char row;
    char seat;
};

void flyerinput(flyerinfotype &flyer);
void request(char seats[maxrow][maxcol], flyerinfotype &flyer);
void printmani(flyerinfotype flyer[], int);
void pause();

int main() {

    flyerinfotype flyer[15];
    char seats[maxrow][maxcol] = {{'A', 'B', 'C'},
                                  {'A', 'B', 'C'},
                                  {'A', 'B', 'C'},
                                  {'A', 'B', 'C'},
                                  {'A', 'B', 'C'}};
    int option, numflyer, num;

    while (true) {
        system("cls");
        cout << " __  _\n"
                " \\ `/ |\n"
                "  \\__`!\n"
                "  / ,' `-.__________________\n"
                " '-'\\_____ Friendly Sky's LI`-.\n"
                "    <____()-=O=O=O=O=O=[]====--)\n"
                "      `.___ ,-----,_______...-'\n"
                "           /    .'\n"
                "          /   .'\n"
                "         /  .'         \n"
                "         `-'" << endl;
        cout << "Thank you for flying Friendly Sky's! " << endl;
        cout << "1. Enter Flyers Infomation" << endl;
        cout << "2. Enter Requested Seat" << endl;
        cout << "3. Print Assigned Seats" << endl;
        cout << "4. Print Manifest" << endl;
        cout << "5. Exit" << endl;
        cout << "Please enter menu option 1-5: ";
        while(!(cin >> option) || option <= 0 || option >= 7){ // checks for valid menu entry 1-6
            if ( !cin )
            {
                cout << "ERROR: You have entered an invalid character, please enter a number: ";
                cin.clear();
                cin.ignore(200,'\n');
            }
            else
                cout << "ERROR: The number must be a number 1 - 6, please reenter: ";
        }

        switch (option) {
            case 1: {
                system("cls");
                flyerinput(flyer[numflyer]); // calls the flyer input
                num = 1;
                pause();
                break;
            }
            case 2: {
                system("cls");
                if (num == 0) {
                    cout << "Please make sure you have entered the flyer's information first." << endl;
                    pause();
                }else {
                    request(seats, flyer[numflyer]); // calls the requested seat function
                    numflyer++;
                    pause();
                    num = 0;
                }
                break;
            }
            case 3: { // prints the current seats available
                system("cls");
                cout << setw(12) << "Seats" << endl;
                for (int i = 0; i < maxrow; i++){
                    cout << "Row " << i+1 << ": ";
                    for (int j = 0; j < maxcol; j ++){
                        cout << seats[i][j];
                        cout << " ";
                    }
                    cout << endl;
                }
                pause();
                break;
            }
            case 4: { // prints the manifest
                system("cls");
                printmani(flyer, numflyer);
                pause();
                break;
            }
            case 5: {
                return 0;
            }
        }
    }
}

void flyerinput(flyerinfotype &flyer) {

    int l;
    cout << "Enter the flyer's frist name: ";
    cin >> flyer.fname;
    l = flyer.fname.length();

    for (int i = 1; i < l; i++){
        if(flyer.fname[0] > 'a' || flyer.fname[0] > 'z'){
            flyer.fname[0] = toupper(flyer.fname[0]);
        }
        flyer.fname[i] = tolower(flyer.fname[i]);
    }
    cout << "Enter the flyer's last name: ";
    cin >> flyer.lname;
    for (int i = 1; i < l; i++){
        if(flyer.lname[0] > 'a' || flyer.lname[0] > 'z'){
            flyer.lname[0] = toupper(flyer.lname[0]);
        }
        flyer.lname[i] = tolower(flyer.lname[i]);
    }
    cout << "Enter the flyer's ID number: ";
    cin >> flyer.id;
    l = flyer.id.length();
    while( l <= 0 || l >= 5){
        cout << "ERROR: The ID must be a 4 characters, please reenter: ";
        cin.clear();
        cin.ignore(200,'\n');
        cin >> flyer.id;
        l = flyer.id.length();
    }
    cout << "Enter the flyer's number of bags: ";
    cin >> flyer.numbags;
}

void request(char seats[maxrow][maxcol], flyerinfotype &flyer){

    string s="";

    cout << setw(12) << "Seats" << endl;
    for (int i = 0; i < maxrow; i++){
        cout << "Row " << i+1 << ": ";
        for (int j = 0; j < maxcol; j ++){
            cout << seats[i][j];
            cout << " ";
        }
        cout << endl;
    }
    cout << "Please enter the seat the flyer would like to have (eg: 2A): ";
    cin >> s;
    for (int i = 0; i < 2; i++){
        s[i] = tolower(s[i]);
    }
    while ((s.length()==!2)||s[0] < '1'||s[0] > '5'||s[1] < 'a'||s[1] > 'c'||seats[s[0]-'1'][s[1]-'a']=='X')
    {
        cout << "Error: That seat is unavalible.";
        cout << "Please choose another seat (eg: 2A): ";
        cin >> s;
    }
    seats[s[0]-'1'][s[1]-'a'] = 'X';
    cout << setw(12) << "Seats" << endl;
    for (int i = 0; i < maxrow; i++){
        cout << "Row " << i+1 << ": ";
        for (int j = 0; j < maxcol; j ++){
            cout << seats[i][j];
            cout << " ";
        }
        cout << endl;
    }
    flyer.row = s[0];
    flyer.seat = toupper(s[1]);
    cout << "Seat: " << flyer.row << flyer.seat << " has been selected for flyer ID: " << flyer.id << endl;
}

void printmani(flyerinfotype f[], int n){

    cout << "    Last Name          First Name        ID   #Bags  OP  Seat" << endl;
    for (int i = 0; i < n; i++){
        cout << i+1 << ".  " << left << setw(19) << f[i].lname << setw(17) << f[i].fname << setw(8) << f[i].id << setw(5)
        << f[i].numbags << setw(5) << f[i].row << f[i].seat << endl;
    }
}

void pause(){

    cout << "Press any key to continue ... " << endl;
    getch();

}